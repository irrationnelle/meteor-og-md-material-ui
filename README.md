# About

* [Meteor](https://www.meteor.com/)
* [React](https://github.com/facebook/react)
* [React Router v4](https://github.com/ReactTraining/react-router)
* [Material-UI 1.0(Beta)](https://material-ui-1dab0.firebaseapp.com/)


# Installation

### It is required to install [Meteor](https://www.meteor.com/install), first of all
 
`npm install`

# Run the app

`meteor`